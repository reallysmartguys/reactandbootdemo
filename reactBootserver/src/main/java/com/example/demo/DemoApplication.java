package com.example.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(PersonRepository personRepository){
		return (args)->{

			Arrays.asList("Emmanuel BLONVIA, Roland Reagan, Bill Gates, Jay Gould, Sreenu DOOSARI, Henri Ford, John-D. Rockfeller, Andrew Carnegie"
					.split(","))
					.stream()
					.map(fullName->fullName.trim().split(" "))
					.map(fullName-> new Person(fullName[0], fullName[1]))
					.forEach(person ->  personRepository.save(person));

			personRepository.findAll().stream().forEach(System.out::println);
		};


	}

}
