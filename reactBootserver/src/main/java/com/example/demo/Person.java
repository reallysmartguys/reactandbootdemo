package com.example.demo;

import javax.persistence.*;
import java.util.UUID;


/*@Data
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString*/
@Entity
public class Person {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    @Column
    private final String firstName;
    @Column
    private final String lastName;

    public Person() {
        this("", "");
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (!firstName.equals(person.firstName)) return false;
        return lastName.equals(person.lastName);
    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Long getId() {
        //avoiding to expose the real ids for security reasons
        UUID fromId = UUID.nameUUIDFromBytes(String.valueOf(id).getBytes());
        return fromId.getLeastSignificantBits() ^ fromId.getMostSignificantBits();
    }
}
