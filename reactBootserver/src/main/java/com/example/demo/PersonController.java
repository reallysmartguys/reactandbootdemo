package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@CrossOrigin(origins = {"http://localhost:3000"})
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @GetMapping("/persons")

    @ResponseBody
    public List<Person> allPersons() {
        System.out.println("Recherche des personnes");
        return personRepository.findAll();
    }


    /* @CrossOrigin(origins = {"http://localhost:3000"})*/
    @PostMapping("/person")
    @ResponseBody
    public Person createOne(@RequestBody Person person) {
        System.out.println("Enregisstrement de personnes" + person);
        Person result = personRepository.save(person);
        System.out.println("Personne restournée  : " + result);
        return result;
    }


}
