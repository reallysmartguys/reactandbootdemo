
import React from 'react';


const Identification =({onChangeLastName,onChangeFirstName,onEnregistrer,firstName,lastName, message})=> 
    <div>
        <h1> Formulaire inscription</h1>
            {message &&  <div style={{color:'red'}}><h1> {message}</h1></div> }
        <form>
            <label htmlFor="lastName">Nom </label>
            <input onChange={onChangeLastName} name="lastName" type='text' value={lastName}  />
            <label htmlFor="firsName">Prenom </label>
            <input onChange={onChangeFirstName} name="firstName" type='text' value={firstName}  />
            <input type='submit' onClick={onEnregistrer} value='Enregistrer' />
        </form>
    </div>
        


export default Identification;