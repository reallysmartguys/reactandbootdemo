
import React from 'react';
const ListPerson = (props)=> (
    <div>
        <h5> Most Important People On EARTH : </h5>
        <ol>
            {props.data && props.data.map(element =>
                <li key={element.id}>{element.firstName} {element.lastName}</li>
            )}
        </ol>
    </div>
)

export default ListPerson;