import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Identification from './Identification';
import ListPerson from './ListPerson';
import axios from 'axios'

class App extends React.Component {


  constructor(props){
    super(props);
    this.state = {
      personnes :[],
      identcationData : {
        firstName:'',
        lastName:''
      },
      message:''
    };

    this.envoyer = this.envoyer.bind(this);
  }
  
componentDidMount(){

  fetch('http://localhost:8888/persons')
  .then(data=>data.json())
  .then(data=>{
    console.log("fetched data : ",data)
    this.setState(
    {
      personnes:data
    }
  )}
  ).catch(err=>console.log("Euuh, ya erreur : ",err))
}
  
//  setLastName = (event) => {
//     this.setState({
//       identcationData :{...this.state.identcationData,lastName:event.target.value}
//     })
//  }

//  setFirstName = (event) => {
//    this.setState({
//     identcationData : {...this.state.identcationData,firstName:event.target.value}
//    })
//  }

  setFormData=name=>event=>{
   let temp = {};
      temp[name] = event.target.value;
      this.setState({
       identcationData : {...this.state.identcationData,...temp}
      })
    
  }

  envoyer (event) {
    event.preventDefault();
    let {identcationData} = this.state;
    let messageError=undefined;

    if(!identcationData.firstName && !identcationData.lastName) {
      messageError ="BOOM ! TRY AGAIN, YOU CAN SUBMIT EMPTY FORM  !"
    }
    else {
      
    let {firstName,lastName} = identcationData;
    axios.post('http://localhost:8888/person', {
      firstName,
      lastName
    })
    .then((response)=> 
      this.setState({personnes: [...this.state.personnes, response.data]})
    )
    .catch( (error)=> 
      console.log(" Erreur au retour de axios :", error)
    );
  }
    this.setState({
        identcationData: {
          firstName:'',
          lastName:''
        },
        message:messageError
    })
   
}


  render() {
    let { personnes, identcationData, message} = this.state;
    console.log("identcationData : ", identcationData )
    return (
      <div>
        <Identification 
          onEnregistrer={this.envoyer} 
          onChangeFirstName={this.setFormData('firstName')}
          onChangeLastName={this.setFormData('lastName')}
          firstName={identcationData.firstName}
          lastName={identcationData.lastName}
          message={message}
        />
        <ListPerson data={personnes} />
      </div>
      
    )
  }
}


ReactDOM.render(<App />,
  document.getElementById('root')
);


